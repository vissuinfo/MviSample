package com.canure.mvisample

import io.reactivex.Observable
import io.reactivex.functions.Consumer

interface MainView {

    fun getActions() : Map<String, Observable<in String>>
    fun getConsumers() : Map<String, Consumer<in String>>
}