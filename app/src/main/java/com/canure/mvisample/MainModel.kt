package com.canure.mvisample

import io.reactivex.functions.Consumer

class MainModel(var consumers: Map<String, Consumer<in String>>) {

    fun changeText(text: String) {
        consumers.get("TextView")!!.accept(text)
    }
}
