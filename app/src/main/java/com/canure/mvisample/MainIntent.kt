package com.canure.mvisample

import io.reactivex.Observable

class MainIntent(var mainView : MainView) {

    private var actions: Map<String, Observable<in String>> = mainView.getActions()
    private var mainModel : MainModel = MainModel(mainView.getConsumers())

    fun start() {
        actions.get("EditText")?.subscribe{ text -> mainModel.changeText(text.toString()) }
     }
}