package com.canure.mvisample

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.Observable
import io.reactivex.functions.Consumer

class MainActivity : AppCompatActivity(), MainView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        MainIntent(this).start()
    }

    override fun getConsumers(): Map<String, Consumer<in String>> {
        var consumers = HashMap<String, Consumer<in String>>()
        consumers.put("TextView", RxTextView.text(findViewById(R.id.txtView)))
        consumers.put("EditText", RxTextView.text(findViewById(R.id.editText)))
        return consumers
    }

    override fun getActions(): Map<String, Observable<in String>> {
        var actions = HashMap<String, Observable<in String>>()
        actions.put("EditText", RxTextView.textChanges(findViewById(R.id.editText)))
        return actions
    }

}
