package com.canure.mvisample

import io.reactivex.functions.Consumer
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun `should display text`() {
        var consumers : HashMap<String, Consumer<in String>> = HashMap()
        consumers.put("TextView", Consumer<String> {
            assertEquals(it, "vissu")
        })
        var state = MainModel(consumers)
        state.changeText("vissu")
    }
}
